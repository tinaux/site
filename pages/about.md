---
layout: page
title: About
permalink: /about/
feature-img: "assets/img/pexels/projects.jpeg"
tags: [About]
---

Hi!

So a little bit about me..

I moved to London 3 years ago. I am originary from Romania, Transylvania... and I hate garlic...

My hobbies are much the same as everyone else: travelling, exploring new places, spending time in nature, social media and reading. I always welcome new challenges and I like to believe I am an active person.

I have worked in Customer Service for 4 years and then moved to Web Content and UX 2 years ago. 

I got into UX because analysing people's behaviour fascinates me. I want to make things easier, more intuitive, create useful features and solve problems that make our daily life difficult.