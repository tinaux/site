---
layout: post
title: User Interface Design Principles
feature-img: "assets/img/pexels/desk-messy.jpeg"
thumbnail: "assets/img/thumbnails/desk-messy.jpeg"
tags: [User Experience]
---
This is an example of a post which includes a feature image specified in the front matter of the post. The feature image spans the full-width of the page, and is shown with the title on permalink pages.

First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

![Geometric pattern with fading gradient]({{ site.baseurl }}/assets/img/sample2.png)