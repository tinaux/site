---
layout: post
title: UX design for VR
feature-img: "assets/img/portfolio/screenshot1.png"
img: "assets/img/portfolio/hackathon.png"
date: 2017-07-20
tags: [MobileUX, VR, Teamwork, Hackday]
---
  I went to my first hackathon last weekend. It was so insipiring... I met some great people with amazing backgrounds, learnt so many things and even managed to win a prize. 
  
  The hackathon was hosted by <a href="https://mobileuxlondon.com/hackux-vr-hackathon-orange/" target="_blank">MUXL (Mobile UX London)</a>, Orange and <a href="https://halolabs.com/ " target="_blank">Halo Labs</a> and we had the chance to develop an idea for a mobile VR app. We were given 3 briefs and we chose to pitch an idea for B... 

**B**

*Exploit dynamic and multi-modal attributes of the VR environments. Explore using sounds, lighting, texture and amorphous elements (swarms, clouds, etc.) within the VR environment to facilitate the user with orientation or a directed activity, such as wayfinding towards a location, object or person. [Brief probably needs to propose more specific goals/tasks, to be worked on].* 

#### My Team: 
"Team FEAR" - consisted of 6 members, each of us with varied backgrounds.

#### Our idea: 
Create a mobile VR app that would solve a social problem -  "**PhoGO**" - a VR app that will help people to overcome public speaking fear.

Most of the people suffer from public speaking phobia and they either spend a lot of money on therapies to overcome it, or limit their career opportunities by avoinding public speaking situations.

PhoGO will help them getting better at public speaking by practising in the comfort of their home at a smaller cost and no time limit.

#### Our process:

**Warming up session**: Everybody was so excited about this opportunity, for most of us being the first hackathon we've ever been to. To break the ice we started talking about our work experience, our skills, passions and slowly became to know each other better.

**Brainstorming session**: From friendly chatting, we moved to our main purpose and decided upon an action plan. 
Tasks were assigned to every member according to our skills. We set up catch up meetings every hour and defined specific timelines for different stages of the project.

![Brainstorming session]({{ "/assets/img/brainstorming.jpg" | absolute_url }})


**Discovery (Research)**: 
We shared personal stories about fear, gathered ideas and organised a couple of one-to-one interviews with members from other teams. Then we started to create a user story.

The team did some research online and gathered valuable data about the impact anxiety has on people. 
*Public speaking (Glossophobia) seems to be the greatest phobia for the majority of the population (75% of the US population suffer from it.)*

Then we dipped further and discovered different therapies people try to overcome it. VR seems to have already been used by doctors in therapies for other extreme phobias. So we had the proof that our app is going to solve an issue that impacts a large group of users in an innovative way.

**Analysis**: All ideas, observations, experiences were written down on post it notes and used to create a <a href="https://www.smashingmagazine.com/2014/01/how-working-walls-unlock-creative-insight/" target="_blank">"wall of information"</a> . This helped us to draw connections between different elements, develop new and deeper insights and to put together a clear problem-defining process and develop potential solutions.

![Wall of information]({{ "/assets/img/wall.png" | absolute_url }})

**Ideation**: Once all these data have been well structured, we focused on creating a user persona, a clear journey map and identifying major pain points that we had to tackle. 

 ![User Persona]({{ "/assets/img/userpersona.png" | absolute_url }})

**Design**: By the end of the day we also had to create a basic prototype that would sustain our idea.
Halo is a new VR prototyping software that was introduced to us a day before. We had a short training online and we were ready to try it for our prototype.

![1st scene]({{ "/assets/img/hallo1.png" | absolute_url }})
![2nd scene]({{ "/assets/img/hallo2.png" | absolute_url }})

As any new tool, we had some issues and thought we might have lost all our work 1 hour before the presentation. A bit of panic and frustration, but everything was fixed eventually. I tend to believe I fixed it by deleting the numerous projects everybody created in the database for testing, occupying too much space. I don't have the confirmation though...

**Implementation**: In the end we put together a PowerPoint presentation that would contain a short description of our app and the design thinking process we used to create it.

The day ended with smiles on our faces as we got the prize for Most Original Concept. 


![Prize]({{ "/assets/img/prize.jpeg" | absolute_url }})


#### The Take Away:

It was a long hard working day, where everybody had one purpose - create a unique innovative product. 

Even though we knew each other for a couple of hours only, the desire to do something great resulted in an amazing team work. 
There was no judgement or criticism, no bad ideas. Everybody could express their point of views while others would be listening.
There was discipline (one conversation at a time, well defined timelines and project steps), collaboration and communication. We didn't loose track of our work and were able to finish on time and prepare for the presentation. 

I believe this was the key for our success and it is for any successful project.

We learnt a lot from each other and our efforts were appreciated and rewarded.
