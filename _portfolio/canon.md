---
layout: post
title: Canon B2B Support Site
feature-img: "assets/img/portfolio/canon-lens.png"
img: "assets/img/portfolio/canon-lens.png"
date: 2017-09-03
tags: [User Experience, Agile, Content strategy]
---
 

### Intro

Canon website is divided into 3 categories: Business, Consumer and Pro Photo & Video. 
Each of these sites has 3 main sections: Marketing, Support and E-commerce.

As web experience specialist I managed the web experience for the <a href="https://www.canon.co.uk/support/business-product-support/" target="_blank">Business Support site</a> across 28 EMEA countries. 
The Support section is mainly visited by existent customers who have already purchased a product and need information or help to use it. The support area gets 50% of the total visits to the Canon website and plays an important role in building up customers fidelity and creating word-of-mouth advertising. 

### Issues
The platform for Business Support section was very old; the customer journey was far from ideal and the CMS didn't allow us to improve the user experience on the website. 

The only task we could perform was to publish or delete content. 


### Research
Given the above issues, we decided to do some investigation and research to understand what options we have for solving it. 

As feedback from users and stakeholders is crucial when you’re trying to get from your existing design to the next iteration of it, we started getting in touch with different teams within our company. 
This helped us a lot as we got a better understanding of the huge impact the very old platform had on users and the urgency for improving it.

- We checked with the IT support team if there could be an upgrade of the old CMS (SDS) that would offer us more capabilities. Or if content migration to the CMS (Tridion) we were using for the Consumer site would be a more viable option.
- We approached different stakeholders (Marketing teams, Engineers, Product Specialists) to check their experience with the current platform.
- We asked Contact Centre, the main contact point for our customers, to provide us with an overview of the feedback agents got from customers.

### Proposal
After gathering all this valuable feedback we put together a roadmap for a long-term project. It required extra budget, internal procedures change and creation of users stories for the functional changes. 
We defined the main steps and grouped them in smaller projects as listed below:

##### Improve content strategy
- Migrate B2B content to Tridion (the CMS we use for the Consumer site) and make it available across 28 EMEA websites
- Request translation of dynamic content in 10 languages
- Set up automatic publishing and translation rules for dynamic content

##### Improve user experience 
- Create a home page and landing pages for all 28 sites 
- Create a usable template for the product pages: content tabs, automatic OS detection, 'see more...' module for useful relevant content
- Implement a survey across all sites
- Add Google Analytics to the site

### Agile
During build I took over and worked closely with developers and designers to implement each change successfully. 
Every week we would have skype meetings with the whole team (front end, back end, design and UX) and score each user story by effort and difficulty, process known as 'Agile Poker'. The points were used later to define deadlines and priority. 

Some changes were sometimes split in more user stories, that would allow the developers to deploy different projects in the same time.
I also worked closely with the develoers to test and ensure designs were aligned with the wireframes and the performance of the website was not affected. 

### Outcome

The user experience on Canon B2B Support site has been improved considerably and the new CMS allows the UX team to fix other issues in the future.
 
![BEFORE-AFTER]({{ "/assets/img/portfolio/before-after1.png" | absolute_url }}) 
![BEFORE-AFTER]({{ "/assets/img/portfolio/text.png" | absolute_url }})


